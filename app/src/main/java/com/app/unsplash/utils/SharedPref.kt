package com.app.unsplash.utils

import com.chibatching.kotpref.KotprefModel

/**
 * Created by Fariz Ramadhan.
 * website : https://farizdotid.com/
 * github : https://github.com/farizdotid
 * linkedin : https://www.linkedin.com/in/farizramadhan/
 */
class SharedPref {
    object User : KotprefModel() {
        var isLogin by intPref(default = 0)
    }
}
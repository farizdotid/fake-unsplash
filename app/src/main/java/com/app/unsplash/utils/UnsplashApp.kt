package com.app.unsplash.utils

import android.app.Application
import com.chibatching.kotpref.Kotpref
import ss.com.bannerslider.Slider

/**
 * Created by Fariz Ramadhan.
 * website : https://farizdotid.com/
 * github : https://github.com/farizdotid
 * linkedin : https://www.linkedin.com/in/farizramadhan/
 */
class UnsplashApp : Application() {
    override fun onCreate() {
        super.onCreate()
        Kotpref.init(this)
        Slider.init(GlideImageLoadingService(this))
    }
}
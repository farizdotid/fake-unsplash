package com.app.unsplash.utils

import ss.com.bannerslider.adapters.SliderAdapter
import ss.com.bannerslider.viewholder.ImageSlideViewHolder

/**
 * Created by Fariz Ramadhan.
 * website : https://farizdotid.com/
 * github : https://github.com/farizdotid
 * linkedin : https://www.linkedin.com/in/farizramadhan/
 */
class MainSliderAdapter(private val images: ArrayList<String>) : SliderAdapter() {

    override fun onBindImageSlide(position: Int, imageSlideViewHolder: ImageSlideViewHolder) {
        val url = images[position]
        imageSlideViewHolder.bindImageSlide(url)
    }

    override fun getItemCount(): Int {
        return images.size
    }

}
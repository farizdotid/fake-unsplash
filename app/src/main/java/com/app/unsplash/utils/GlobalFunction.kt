package com.app.unsplash.utils

import android.content.Context
import android.util.Log
import android.widget.ImageView
import com.app.unsplash.R
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.signature.ObjectKey

/**
 * Created by Fariz Ramadhan.
 * website : https://farizdotid.com/
 * github : https://github.com/farizdotid
 * linkedin : https://www.linkedin.com/in/farizramadhan/
 */
object GlobalFunction {
    fun debug(message: String) {
        Log.d("--- debug ---", message)
    }

    fun loadImage(context:Context, source : Any, imageView: ImageView){
        GlideApp.with(context)
            .load(source)
            .placeholder(R.color.colorPrimaryDark)
            .signature(ObjectKey(System.currentTimeMillis() / (24 * 60 * 60 * 1000)))
            .transform(CenterCrop())
            .into(imageView)
    }

    fun loadImageCircle(context:Context, source : Any, imageView: ImageView){
        GlideApp.with(context)
            .load(source)
            .placeholder(R.color.colorPrimaryDark)
            .signature(ObjectKey(System.currentTimeMillis() / (24 * 60 * 60 * 1000)))
            .transform(CenterCrop(), CircleCrop())
            .into(imageView)
    }
}
package com.app.unsplash.utils

/**
 * Created by Fariz Ramadhan.
 * website : https://farizdotid.com/
 * github : https://github.com/farizdotid
 * linkedin : https://www.linkedin.com/in/farizramadhan/
 */
object Constant {
    val EXTRA_DATA_NAME_CATEGORY = "extra_data_name_category"
}
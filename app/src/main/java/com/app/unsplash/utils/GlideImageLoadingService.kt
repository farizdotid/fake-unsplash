package com.app.unsplash.utils

import android.content.Context
import android.widget.ImageView
import ss.com.bannerslider.ImageLoadingService

/**
 * Created by Fariz Ramadhan.
 * website : https://farizdotid.com/
 * github : https://github.com/farizdotid
 * linkedin : https://www.linkedin.com/in/farizramadhan/
 */
class GlideImageLoadingService(val context: Context) : ImageLoadingService {

    override fun loadImage(url: String?, imageView: ImageView) {
        GlobalFunction.loadImage(context, url.toString(), imageView)
    }

    override fun loadImage(resource: Int, imageView: ImageView) {
        GlobalFunction.loadImage(context, resource, imageView)
    }

    override fun loadImage(
        url: String?,
        placeHolder: Int,
        errorDrawable: Int,
        imageView: ImageView?
    ) {
    }

}
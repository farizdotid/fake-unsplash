package com.app.unsplash.ui.fragment


import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager

import com.app.unsplash.R
import com.app.unsplash.domain.local.Category
import com.app.unsplash.ui.activity.DetailCategoryActivity
import com.app.unsplash.ui.adapter.CategoryAdapter
import com.app.unsplash.utils.Constant
import kotlinx.android.synthetic.main.fragment_home.*

/**
 * A simple [Fragment] subclass.
 */
class HomeFragment : Fragment(), CategoryAdapter.CategoryAdapterCallback {

    private lateinit var categoryAdapter: CategoryAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        initRecyclerview()
    }

    private fun initRecyclerview(){
        categoryAdapter = CategoryAdapter(requireContext(), getCategoryData(), this)
        rvCategory.layoutManager = LinearLayoutManager(activity)
        rvCategory.itemAnimator = DefaultItemAnimator()
        rvCategory.adapter = categoryAdapter
    }

    private fun getCategoryData() : ArrayList<Category> {
        val list = ArrayList<Category>()
        list.add(Category("Food", "https://images.unsplash.com/photo-1569420077790-afb136b3bb8c?ixlib=rb-1.2.1&auto=format&fit=crop&w=767&q=80"))
        list.add(Category("Nature", "https://images.unsplash.com/photo-1572295727871-7638149ea3d7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80"))
        list.add(Category("Travel", "https://images.unsplash.com/photo-1571627912808-5183884c6b8c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80"))
        return list
    }

    override fun onRowCategoryAdapterClicked(position: Int) {
        val nameCategory = getCategoryData()[position].name.toLowerCase()

        val intent = Intent(activity, DetailCategoryActivity::class.java)
        intent.putExtra(Constant.EXTRA_DATA_NAME_CATEGORY, nameCategory)
        startActivity(intent)
    }
}

package com.app.unsplash.ui.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.Toast
import com.app.unsplash.R
import com.app.unsplash.utils.SharedPref
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val isLogin = SharedPref.User.isLogin
        if (isLogin == 1){
            startActivity(Intent(this, DashboardActivity::class.java))
        }

        btnLogin.setOnClickListener {
            val email = etEmail.text.toString()
            val password = etPassword.text.toString()

            if (TextUtils.isEmpty(email) || TextUtils.isEmpty(password)) {
                Toast.makeText(
                    this,
                    getString(R.string.error_message_form_empty),
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                SharedPref.User.isLogin = 1
                startActivity(Intent(this, DashboardActivity::class.java))
                finish()
            }
        }

    }
}
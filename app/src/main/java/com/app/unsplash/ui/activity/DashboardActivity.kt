package com.app.unsplash.ui.activity

import android.os.Bundle
import android.view.MenuItem
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.app.unsplash.R
import com.app.unsplash.ui.fragment.HomeFragment
import com.app.unsplash.ui.fragment.ProfileFragment
import kotlinx.android.synthetic.main.activity_dashboard.*

class DashboardActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)

        initFragment(HomeFragment(), "home")
        initBottomNav()
    }

    private fun initBottomNav() {
        bottomNav.setOnNavigationItemSelectedListener(object :
            BottomNavigationView.OnNavigationItemSelectedListener {
            override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {
                when (menuItem.itemId) {
                    R.id.navHome -> {
                        initFragment(HomeFragment(), "home")
                        return true
                    }
                    R.id.navProfile -> {
                        initFragment(ProfileFragment(), "profile")
                        return true
                    }
                }
                return false
            }
        })
    }

    private fun initFragment(fragment: Fragment, tag: String) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frameFragment, fragment, tag)
        transaction.commit()
    }
}
package com.app.unsplash.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.app.unsplash.R
import com.app.unsplash.utils.Constant
import com.app.unsplash.utils.MainSliderAdapter
import kotlinx.android.synthetic.main.activity_detail_category.*
import ss.com.bannerslider.adapters.ImageSliderAdapter

class DetailCategoryActivity : AppCompatActivity() {

    private var sNameCategory: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_category)

        sNameCategory = intent.getStringExtra(Constant.EXTRA_DATA_NAME_CATEGORY)
        initSlider(sNameCategory)
    }

    private fun initSlider(nameCategory: String) {
        when (nameCategory) {
            "food" -> {
                bannerSlider.setAdapter(MainSliderAdapter(getDataFood()))
            }
            "nature" -> {
                bannerSlider.setAdapter(MainSliderAdapter(getDataNature()))
            }
            "travel" -> {
                bannerSlider.setAdapter(MainSliderAdapter(getDataTravel()))
            }
        }

    }

    private fun getDataFood(): ArrayList<String> {
        val list = ArrayList<String>()
        list.add("https://images.unsplash.com/photo-1565396319243-49b1f67c82a1?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=751&q=80")
        list.add("https://images.unsplash.com/photo-1568093858174-0f391ea21c45?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=334&q=80")
        list.add("https://images.unsplash.com/photo-1568376794508-ae52c6ab3929?ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80")
        return list
    }

    private fun getDataNature(): ArrayList<String> {
        val list = ArrayList<String>()
        list.add("https://images.unsplash.com/photo-1571847490051-491c12ff6540?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80")
        list.add("https://images.unsplash.com/photo-1571586100122-0869bd6e77c9?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80")
        list.add("https://images.unsplash.com/photo-1571217668979-f46db8864f75?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80")
        return list
    }

    private fun getDataTravel(): ArrayList<String> {
        val list = ArrayList<String>()
        list.add("https://images.unsplash.com/photo-1571497569639-7bd0fcd36c64?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=334&q=80")
        list.add("https://images.unsplash.com/photo-1571427978027-75c8bde0b6bc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=375&q=80")
        list.add("https://images.unsplash.com/photo-1571655727442-d73723546245?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=355&q=80")
        return list
    }
}

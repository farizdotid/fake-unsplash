package com.app.unsplash.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import androidx.recyclerview.widget.RecyclerView

import com.app.unsplash.R
import com.app.unsplash.domain.local.Category
import com.app.unsplash.utils.GlobalFunction
import kotlinx.android.synthetic.main.item_category.view.*

/**
 * Created by Fariz Ramadhan.
 * website : https://farizdotid.com/
 * github : https://github.com/farizdotid
 * linkedin : https://www.linkedin.com/in/farizramadhan/
 */

class CategoryAdapter(
    private val context: Context,
    private val list: ArrayList<Category>,
    private val mAdapterCallback: CategoryAdapterCallback?
) : RecyclerView.Adapter<CategoryAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.item_category,
            parent, false
        )
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val name = list[position].name
        val imageUrl = list[position].imageUrl

        holder.itemView.tvName.text = name
        GlobalFunction.loadImage(context, imageUrl, holder.itemView.ivImage)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun clear() {
        val size = this.list.size
        this.list.clear()
        notifyItemRangeRemoved(0, size)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        init {
            itemView.setOnClickListener { mAdapterCallback?.onRowCategoryAdapterClicked(adapterPosition)}
        }
    }

    interface CategoryAdapterCallback {
        fun onRowCategoryAdapterClicked(position: Int)
    }

    companion object {
        private val TAG = CategoryAdapter::class.java.simpleName
    }
}
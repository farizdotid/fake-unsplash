package com.app.unsplash.ui.fragment


import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.app.unsplash.R
import com.app.unsplash.ui.activity.DashboardActivity
import com.app.unsplash.ui.activity.LoginActivity
import com.app.unsplash.utils.GlobalFunction
import com.app.unsplash.utils.SharedPref
import kotlinx.android.synthetic.main.fragment_profile.*

/**
 * A simple [Fragment] subclass.
 */
class ProfileFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val urlImage = "https://secure.meetupstatic.com/photos/member/b/f/3/5/highres_269328949.jpeg";
        GlobalFunction.loadImageCircle(requireContext(), urlImage, ivAvatar)

        btnLogout.setOnClickListener {
            SharedPref.User.isLogin = 0
            startActivity(Intent(activity, LoginActivity::class.java))
            activity?.finish()
        }
    }
}
